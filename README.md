# README
------

## References

Beckstein, P.; Galindo, V.; Vukčević, V.: Efficient solution of 3D electromagnetic eddy-current problems within the finite volume framework of OpenFOAM, Journal of Computational Physics, Volume 344, 1 September 2017, Pages 623-646

http://dx.doi.org/10.1016/j.jcp.2017.05.005

Beckstein, P.; Galindo, V.; Gerbeth, G.: (2019) Free-Surface Dynamics in Induction Processing Applications. In: Nóbrega J., Jasak H. (eds) OpenFOAM®. Springer, Cham.

https://doi.org/10.1007/978-3-319-60846-4_15

Beckstein, P.: Methodenentwicklung zur Simulation von Strömungen mit freier Oberfläche unter dem Einfluss elektromagnetischer Wechselfelder (2018) PhD Thesis

https://nbn-resolving.org/urn:nbn:de:bsz:14-qucosa-232474

## Installation
------

### Pre-requirements

#### Ubuntu
##### Ubuntu 20.04 <a name="ubuntu2004"></a>
```bash
sudo apt-get update

sudo apt-get install git-core build-essential binutils-dev cmake flex \
zlib1g-dev libncurses5-dev curl bison \
libxt-dev rpm mercurial graphviz python python-dev  gcc-7 g++-7
```

### Download/Clone root repository

```bash
cd "$HOME"
 
git clone https://gitlab.hzdr.de/galindo/foam-extend foam
```

### Clone foam-extend-4.1 as submodule
```bash
cd "$HOME/foam"
 
git submodule update --init --recursive
```


### Project part

#### Prepare preferences from template
```bash
cd "$HOME/foam/foam-extend-4.1"
#
# Template
cp 'etc/prefs.sh-EXAMPLE' 'etc/prefs.sh'
# System qt
perl -i -pe "s{^(export QT_THIRD_PARTY=1)}{#\1}" 'etc/prefs.sh'
perl -i -pe "s{(#export QT_BIN_DIR=(\\$)QT_DIR/bin)}{\1\nunset QT_THIRD_PARTY\nexport QT_BIN_DIR=$(dirname $(which qmake))}" 'etc/prefs.sh'
# Thirparty qt
perl -i -pe "s{#(export QT_THIRD_PARTY=1)}{\1}" 'etc/prefs.sh'
# Paraview 5.4.1
perl -i -pe "s{^(export WM_THIRD_PARTY_USE_CMAKE_322=1)}{#\1}" 'etc/prefs.sh'
perl -i -pe "s{#(export WM_THIRD_PARTY_USE_CMAKE_332=1)}{unset WM_THIRD_PARTY_USE_CMAKE_322\n\1}" 'etc/prefs.sh'
perl -i -pe "s{^(export WM_THIRD_PARTY_USE_QT_486=1)}{#\1}" 'etc/prefs.sh'
perl -i -pe "s{#(export WM_THIRD_PARTY_USE_QT_580=1)}{unset WM_THIRD_PARTY_USE_QT_486\n\1}" 'etc/prefs.sh'
perl -i -pe "s{^(export WM_THIRD_PARTY_USE_PARAVIEW_440=1)}{#\1}" 'etc/prefs.sh'
perl -i -pe "s{#(export WM_THIRD_PARTY_USE_PARAVIEW_541=1)}{unset WM_THIRD_PARTY_USE_PARAVIEW_440\n\1}" 'etc/prefs.sh'
cd -
```
#### Source environment
```bash
cd "$HOME/foam/foam-extend-4.1"

echo "export WM_CC='gcc-7'"  >> etc/prefs.sh
echo "export WM_CXX='g++-7'"  >> etc/prefs.sh
 
source 'etc/bashrc'
```
#### Install and configure ccache (optional)
```bash
sudo apt-get install ccache
export PATH="/usr/lib/ccache:${PATH}"
```
#### Compilation/Installation
```bash
cd "$HOME/foam/foam-extend-4.1"
 
sed -i -e 's=rpmbuild --define=rpmbuild --define "_build_id_links none" --define=' ThirdParty/tools/makeThirdPartyFunctionsForRPM
sed -i -e 's/gcc/\$(WM_CC)/' wmake/rules/linux64Gcc/c
sed -i -e 's/g++/\$(WM_CXX)/' wmake/rules/linux64Gcc/c++

./Allwmake.firstInstall | tee 'Allwmake.firstInstall.log'
./Allwmake
```


### User part

#### Source environment
```bash
cd "$HOME/foam"
ln -s 'user-4.1' "$USER-4.1"
cd "$USER-4.1"
#
export FE41_FOAM_INST_DIR="$HOME/foam"
export FE41_FOAM_ETC="$FE41_FOAM_INST_DIR/foam-extend-4.1/etc"
export FE41_FOAM_USER_ETC="$HOME/foam/$USER-4.1/etc"
source "$FE41_FOAM_USER_ETC/bashrc"
fe41
```
#### Compilation/Installation
```bash
cd "$HOME/foam/$USER-4.1"
#
./Allwmake.firstInstall | tee 'Allwmake.firstInstall.log'
./Allwmake
```
